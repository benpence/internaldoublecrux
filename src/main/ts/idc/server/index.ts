import * as express from 'express'
import * as path from 'path'
import * as route from "idc/server/route"

const hostname = process.argv[2];
const port = Number.parseInt(process.argv[3]);
const staticDir = path.join(process.cwd(), process.argv[4]);

const app: express.Application = express();

app.use('/static', route.statics(staticDir))

app.use('/', route.root(staticDir))

app.listen(port, hostname, () => {
  console.log(`Listening on ${hostname}:${port}`);
})