import * as express from 'express'
import * as path from 'path'
import * as serveStatic from 'serve-static'

const staticOptions: serveStatic.ServeStaticOptions = {
  index: false,
}

export const statics = (staticDir: string): express.Handler => {
  return express.static(staticDir)
}

export function root(staticDir: string): express.Handler {
  return (req: express.Request, res: express.Response) => {
    res.sendFile(path.join(staticDir, "index.html"))
  }
}