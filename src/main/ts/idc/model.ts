export enum Side {
  Left = "Left",
  Right = "Right",
}

export interface Message {
  text: string,
  side: Side,
}

export const emptySide = Side.Left

export interface NewMessage {
  text: string,
}
export type Action = NewMessage | "SwitchSides" | "Delete"

export class Conversation {
  messages: Array<Message>
  currentSide: Side

  constructor(messages: Array<Message>, currentSide: Side) {
    this.messages = messages
    this.currentSide = currentSide
  }

  public handleAction = (action: Action): Conversation => {
    if ((<NewMessage>action).text !== undefined) {
      console.log("NewMessage", action)
      const newText = (<NewMessage>action).text

      return this.pushMessage({ text: newText, side: this.currentSide })

    } else if (action === "SwitchSides") {
      console.log("SwitchSides", action)
      return new Conversation(
        this.messages,
        this.currentSide === Side.Left ? Side.Right : Side.Left
      )

    } else if (action === "Delete") {
      console.log("Delete", action)
      return this.popMessage()
    } else {
      throw action
    }
  }

  pushMessage = (message: Message): Conversation => {
    return new Conversation(
      [...this.messages, message],
      message.side,
    )
  }

  peekMessage = (): Message | null =>
    this.messages.length > 0
      ? this.messages[this.messages.length - 1]
      : null

  popMessage = (): Conversation => {
    if (this.messages.length > 0) {
      const totalMessages = this.messages.length
      const lastMessage = this.messages[totalMessages - 1]

      return new Conversation(
        this.messages.slice(0, this.messages.length - 1),
        lastMessage.side,
      )
    } else {
      return this
    }
  }
}

export const emptyConversation = new Conversation([], emptySide)