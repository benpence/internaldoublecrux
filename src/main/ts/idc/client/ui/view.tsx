import * as React from "react"
import * as model from "idc/model"

interface Key { key: string }
type UIAction = model.Action | Key

type State = {
  conversation: model.Conversation,
  uncommittedText: string,
}

const emptyState: State = {
  conversation: model.emptyConversation,
  uncommittedText: "",
}

const isKey = (action: UIAction): action is Key => {
  return (action as Key).key !== undefined
}


export const App = () => {
  const [{ conversation, uncommittedText }, setState] = React.useState(emptyState)

  const metaToUIAction = (event: React.KeyboardEvent): UIAction | null => {
    switch (event.key) {
      case "Enter": return { text: uncommittedText }
      case "Alt": return "SwitchSides"
      case "Backspace": return "Delete"
      default: return null
    }
  }

  const keyToUIAction = (event: React.KeyboardEvent): UIAction | null => {
    switch (event.key) {
      case "Enter": return null
      default: return { key: event.key }
    }
  }

  const handleUIAction = (action?: UIAction): void => {
    if (action === null) {
      return
    }

    if (isKey(action)) {
      setState({
        conversation,
        uncommittedText: uncommittedText + action.key,
      })
    } else if (action === "Delete" && uncommittedText.length > 0) {
      setState({
        conversation,
        uncommittedText: uncommittedText.substr(0, uncommittedText.length - 1),
      })
    } else {
      const lastMessage = conversation.peekMessage()
      const newUncommittedNext = action === "Delete" && lastMessage !== null
        ? lastMessage.text
        : ""

      setState({
        conversation: conversation.handleAction(action),
        uncommittedText: newUncommittedNext,
      })
    }
  }

  const focus = (component: HTMLOrSVGElement): void => {
    if (component !== null) {
      component.focus()
    }
  }

  return (
    <div
      id="app"
      onKeyDown={event => handleUIAction(metaToUIAction(event))}
      onKeyPress={event => handleUIAction(keyToUIAction(event))}
      tabIndex={0}
      ref={focus}
    >
      <Conversation conversation={conversation} text={uncommittedText} />
    </div>
  )
}

const Conversation = (props: { conversation: model.Conversation, text: string }) => {
  return (
    <div className="conversation">
      <div className="messages">
        {props.conversation.messages.map(message =>
          <Message message={message} currentSide={props.conversation.currentSide} />
        )}
        <Message
          message={{ text: props.text, side: props.conversation.currentSide }}
          currentSide={props.conversation.currentSide}
        />
      </div>
    </div>
  )
}

const Message = (props: { message: model.Message, currentSide: model.Side }) => {
  const side = props.currentSide === props.message.side ? "sender" : "recipient"

  return (
    <div className={`message ${side}`}>
      <div className="message-space" />
      <div className="message-text"><span className="hidden-text">{props.message.side}: </span>{props.message.text}</div>
    </div>
  )
}