import * as React from "react"
import * as ReactDOM from "react-dom"

import * as view from "idc/client/ui/view"

ReactDOM.render(<view.App />, document.getElementById("root"))