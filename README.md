# Install

```
$ npm install
```

# Run

```
$ npm run client-build
$ npm run server-prod
```

Visit `http://localhost:3000` to try out.
